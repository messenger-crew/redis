#Messenger MYSQL server

##How to run

- Install Docker on your device
- ```git clone git@bitbucket.org:messenger-crew/redis.git```
- ```cd ./mysql```
- ```./bin/docker.sh start```

##How to manage directly
- Check container id ```docker ps```
- Get access to container terminal ```docker exec -ti <container_id> bash```
