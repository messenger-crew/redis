#!/usr/bin/env sh

INFO_FILE=${INFO_FILE:-"app.info"};
DIR=$(pwd)

. $DIR/$INFO_FILE

MAJOR=$(echo $APP_VERSION | cut -d. -f1)
MINOR=$(echo $APP_VERSION | cut -d. -f2)
PATCH=$(echo $APP_VERSION | cut -d. -f3)

case $1 in
    increase)
    case $2 in
        major)
        MAJOR=$(( MAJOR+1 ))
        MINOR=0;
        PATCH=0;
        ;;
        minor)
        MINOR=$(( MINOR+1 ))
        PATCH=0;
        ;;
        patch)
        PATCH=$(( PATCH+1 ))
        ;;
    esac
    NEW_VERSION="${MAJOR}.${MINOR}.${PATCH}"
    sed -i'.bak' "s/APP_VERSION=\"${APP_VERSION}\"/APP_VERSION=\"${NEW_VERSION}\"/" $DIR/$INFO_FILE
    rm $DIR/$INFO_FILE.bak
    ;;
esac

echo "${MAJOR}.${MINOR}.${PATCH}"
